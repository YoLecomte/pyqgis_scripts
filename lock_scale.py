def lock_scale():
    """
    Prevents mapCanvas to be zoomed in at an implausible zoom level for work, block at 10:1 
    """
    current_scale = iface.mapCanvas().scale()
    if current_scale < 10.0:
        iface.mapCanvas().scaleChanged.disconnect(lock_scale)
        iface.mapCanvas().zoomScale(10.0)
        iface.mapCanvas().scaleChanged.connect(lock_scale)
        
iface.mapCanvas().scaleChanged.connect(lock_scale)