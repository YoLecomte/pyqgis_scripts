import random

def create_random_points(bounding_box: QgsRectangle):
    points_number = 50
    point_layer = iface.activeLayer()
    fields = point_layer.fields()
    xmin, xmax = bounding_box.xMinimum(), bounding_box.xMaximum()
    ymin, ymax = bounding_box.yMinimum(), bounding_box.yMaximum()
    
    feats = []
    if not point_layer.isEditable():
        point_layer.startEditing()
    for _ in range(points_number):
        random_x, random_y = random.uniform(xmin, xmax), random.uniform(ymin, ymax)
        geom = pt = QgsGeometry.fromPointXY(QgsPointXY(random_x, random_y))
        feat = QgsFeature()
        feat.setFields(fields)
        feat.setGeometry(geom)
        feats.append(feat)
    point_layer.addFeatures(feats)
        
if __name__ == '__console__':
    #  Create a Bounding Box
    bb = QgsRectangle(QgsPointXY(0,0), QgsPointXY(200,200))
    create_random_points(bb)
    
    #  Zoom to result
    ext = iface.activeLayer().extent()
    iface.mapCanvas().setExtent(ext)