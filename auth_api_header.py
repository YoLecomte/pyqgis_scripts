token = 'whatever-valid-token'
#Store the token in authentication config
auth_manager = QgsApplication.instance().authManager()
config = QgsAuthMethodConfig('APIHeader')
config.setName('API_auth')
config.setConfig('Authorization', f'Token {token}')
_, config = auth_manager.storeAuthenticationConfig(config, True)

# read the token of current instance
auth_manager = QgsApplication.instance().authManager()
available_by_name = {authcfg.name: id for id, authcfg in auth_manager.availableAuthMethodConfigs().items()}
auth_name = 'API_auth'
found, authcfg = auth_manager.loadAuthenticationConfig(available_by_name[auth_name], QgsAuthMethodConfig(), full=True)
print(authcfg.configMap())


