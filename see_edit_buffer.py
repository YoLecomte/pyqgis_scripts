layer = iface.activeLayer()

#print("Changed geometry : " + str(layer.editBuffer().changedGeometries()))
#print("Added Features : " + str(layer.editBuffer().addedFeatures()))
changed_attrs = layer.editBuffer().changedAttributeValues()
for feat, attrs in changed_attrs.items():
    feat = layer.getFeature(feat)
    for index, new_value in attrs.items():
        field_name = layer.fields().field(index).name()
        print(field_name, new_value)